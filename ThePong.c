/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"
#include <stdio.h>
#include <stdlib.h>

typedef enum GameScreen { LOGO, TITLE, PLAYERS, LEVEL, GAMEPLAY, TWO, FOUR, ENDINGONE, ENDINGTWO, ENDINGFOUR } GameScreen;
typedef enum FadeInOut { FadeIn, FadeOut } FadeInOut;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "The Pong";
    
    GameScreen screen = LOGO;
    FadeInOut fade = FadeIn;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    
    //Player1
    Rectangle player;
    player.width = 20;
    player.height = 100;
    player.x = 30;
    player.y = screenHeight/2 - player.height/2;
    int playerSpeedY = 8;
    
    //Life Bars
    int margin = 5;
    
    //P1
    Rectangle backRect1 = {10, 10, 150, 30};
    Rectangle fillRect1 = { backRect1.x + margin, backRect1.y + margin, backRect1.width - (2 * margin), backRect1.height - (2 * margin)};
    Rectangle lifeRect1 = fillRect1;
    Color lifeColor1 = GREEN;
    
    //P2
    Rectangle backRect2 = {screenWidth - 160, 10, 150, 30};
    Rectangle fillRect2 = { backRect2.x + margin, backRect2.y + margin, backRect2.width - (2 * margin), backRect2.height - (2 * margin)};
    Rectangle lifeRect2 = fillRect2;
    Color lifeColor2 = GREEN;

    //Player2 or enemy
    Rectangle enemy;
    enemy.width = 20;
    enemy.height = 100;
    enemy.x = screenWidth - 50 - enemy.width;
    enemy.y = screenHeight/2 - enemy.height/2;
    int enemySpeedY = 8;
    
    //MULTIPLAYER
    //Player1
    Rectangle player1;
    player1.width = 10;
    player1.height = 50;
    player1.x = 30;
    player1.y = screenHeight/4 - player1.height/2;
    int player1SpeedY = 8;
    
    //Player2
    Rectangle player2;
    player2.width = 10;
    player2.height = 50;
    player2.x = 30;
    player2.y = screenHeight/4 + 224 - player2.height/2;
    int player2SpeedY = 8;
    
    //Player3
    Rectangle player3;
    player3.width = 10;
    player3.height = 50;
    player3.x = screenWidth - 50 - player3.width;;
    player3.y = screenHeight/4 - player3.height/2;
    int player3SpeedY = 8;
    
    //Player4
    Rectangle player4;
    player4.width = 10;
    player4.height = 50;
    player4.x = screenWidth - 50 - player4.width;;
    player4.y = screenHeight/4 + 224 - player4.height/2;
    int player4SpeedY = 8;
    
    //P3
    Rectangle backRect3 = {10, screenHeight - 40, 150, 30};
    Rectangle fillRect3 = { backRect3.x + margin, backRect3.y + margin, backRect3.width - (2 * margin), backRect3.height - (2 * margin)};
    Rectangle lifeRect3 = fillRect3;
    Color lifeColor3 = GREEN;
    
    //P4
    Rectangle backRect4 = {screenWidth - 160, screenHeight - 40, 150, 30};
    Rectangle fillRect4 = { backRect4.x + margin, backRect4.y + margin, backRect4.width - (2 * margin), backRect4.height - (2 * margin)};
    Rectangle lifeRect4 = fillRect4;
    Color lifeColor4 = GREEN;
    
    //Ball Position
    Vector2 ballPosition;
    ballPosition.x = screenWidth/2;
    ballPosition.y = screenHeight/2;
    Vector2 ballSpeed;
    ballSpeed.x = 4;
    ballSpeed.y = 4;
    int ballRadius = 15;
    
    Color logoColor = BLACK;
	logoColor.a = 0;
    
    bool pause = false;
    bool blink = false;
    
    char *Txt;
    Vector2 TamanoTxt;
    
    Vector2 RecPause = {(float)screenWidth, (float)screenHeight};
    Vector2 RecPausePos = {(float)0, (float)0};
    
    const int InitialVelocity = 4;
    const int maxVelocity = 15;
    const int gol = 10;
    int iaLinex;
    
    int secondsCounter = 99;
       
    int framesCounter;          // General pourpose frames counter
    
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    Font fontTTF = LoadFont("resources/colder_weather/ColderWeather-Regular.ttf");
    /*
    InitAudioDevice();
    Sound ColSound = LoadSound("resources/palas.wav");
    Sound GolSound = LoadSound("resources/gol.wav");
    Sound PauseSound = LoadSound("resources/pause.wav");*/
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------

        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                framesCounter++;
				switch(fade){
                    case FadeIn:
                    {
                        if (logoColor.a < 255) logoColor.a++;
				
                        if (framesCounter > 300)
                        {
                            fade = FadeOut;
                            framesCounter = 0;
                        }
                    } break;
                case FadeOut:
                {
                    
                    if (logoColor.a <= 255) logoColor.a--;
				
                        if (framesCounter > 250)
                        {
                            screen = TITLE;
                            framesCounter = 0;
                        }
                } break;
                //default: break;
                }
                
            } break;
            case TITLE: 
            {
             // Update TITLE screen data here!
              
                    
                // TODO: Title animation logic.......................(0.5p)
                
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                if (IsKeyPressed (KEY_ENTER)){
                    screen = PLAYERS;
                }
                
            } break;
            case PLAYERS: 
                
                {
                    if (IsKeyPressed (KEY_O)){
                    screen = LEVEL;
                    }
                    
                    if (IsKeyPressed (KEY_T)){
                              
                        screen = TWO;
                    }
                    
                    if (IsKeyPressed (KEY_F)){
                        
                        screen = FOUR;
                    }
                                        
                } break;
            case LEVEL: 
                {
                    if (IsKeyPressed (KEY_E)){
                        
                        screen = GAMEPLAY;
                    }
                    
                    if (IsKeyPressed (KEY_N)){
                        
                        screen = GAMEPLAY;
                    }
                    
                    if (IsKeyPressed (KEY_H)){
                        
                        screen = GAMEPLAY;
                    }
                                        
                } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!
                    framesCounter++;
                    if (framesCounter % 30 == 0)
                    {
                        framesCounter = 0;
                        blink = !blink;
                    }

                // TODO: Ball movement logic.........................(0.2p)
                if (!pause){
                    ballPosition.x += ballSpeed.x;
                    ballPosition.y += ballSpeed.y;
                }
                
                //Velocidad máxima de la bola
                if (ballSpeed.x >= 15) {
                    ballSpeed.x = 15;
                }
                if (ballSpeed.y >= 15) {
                    ballSpeed.y = 15;
                }
                
                // TODO: Player movement logic.......................(0.2p)
                if (!pause) {
           
                   if (IsKeyDown(KEY_W)) {
                        if (player.y >= 0) {
                            player.y -= playerSpeedY;
                        }   
                    }
                    
                    if (IsKeyDown(KEY_S)) {
                        if (player.y <= screenHeight-player.height) { 
                            player.y += playerSpeedY;
                        }       
                    }            
                
                // TODO: Enemy movement logic (IA)...................(1p)
                    
                    
                    if( ballPosition.x > iaLinex){
                        if (enemy.y <= screenHeight-enemy.height){
                            if(ballPosition.y > enemy.y){
                                enemy.y+=enemySpeedY;
                            }
                        }
                        if  (enemy.y >= 0) {
                            if(ballPosition.y < enemy.y){
                                enemy.y-=enemySpeedY;
                            }
                        }
                    }               
                
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                    if (CheckCollisionCircleRec(ballPosition, ballRadius, player)) {
                        //PlaySound(ColSound);
                        if (abs(ballSpeed.x) < maxVelocity){
                            ballSpeed.x *=-1.2;
                            ballSpeed.y *=1.2;
                        } else {            
                            ballSpeed.x *=-1;
                            ballSpeed.y *=1;
                        }
                    }
                
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                    if (CheckCollisionCircleRec(ballPosition, ballRadius, enemy)) {
                        //PlaySound(ColSound);
                        if (abs(ballSpeed.x) < maxVelocity){
                            ballSpeed.x *=-1.1;
                            ballSpeed.y *=1.1;
                        } else {            
                            ballSpeed.x *=-1;
                            ballSpeed.y *=1;
                        }
                        
                    }
                
                // TODO: Collision detection (ball-limits) logic.....(1p)
                    if (ballPosition.x - ballRadius < 0) { 
                        //PlaySound(GolSound);
                        ballPosition.x = screenWidth/2;
                        ballPosition.y = screenHeight/2;
                        ballSpeed.x = InitialVelocity;
                        ballSpeed.y = InitialVelocity;
                        lifeRect1.width -= gol;
                        
                    }
                    if (ballPosition.x + ballRadius > screenWidth) {
                        //PlaySound(GolSound);
                        ballPosition.x = screenWidth/2;
                        ballPosition.y = screenHeight/2;
                        ballSpeed.x = -InitialVelocity;
                        ballSpeed.y = InitialVelocity; 
                        lifeRect2.width -= gol;
                    }
                    
                    if (ballPosition.y - ballRadius < 0) {                   
                        ballSpeed.y *=-1;   
                    }
                    if (ballPosition.y + ballRadius > screenHeight) {
                        ballSpeed.y *=-1;   
                    }
                
                // TODO: Life bars decrease logic....................(1p)
                
                //Está puesto en el collision detection

                // TODO: Time counter logic..........................(0.2p)             
                    framesCounter++;
                    if (framesCounter >= 60)
                    {
                        secondsCounter--;
                        framesCounter = 0;
                    }
                

                // TODO: Game ending logic...........................(0.2p)
                    if (secondsCounter <= 0) screen = ENDINGONE;
                    if (lifeRect1.width <= 0) screen = ENDINGONE;
                    if (lifeRect2.width <= 0) screen = ENDINGONE;

                }
                
                // TODO: Pause button logic..........................(0.2p)
                if (IsKeyPressed(KEY_P)){
                   // PlaySound(PauseSound);
                    pause = !pause;
                }
                
            } break;
            case TWO:
            { 
                // Update GAMEPLAY screen data here!
                    framesCounter++;
                    if (framesCounter % 30 == 0)
                    {
                        framesCounter = 0;
                        blink = !blink;
                    }

                // TODO: Ball movement logic.........................(0.2p)
                if (!pause){
                    ballPosition.x += ballSpeed.x;
                    ballPosition.y += ballSpeed.y;
                }
                
                //Velocidad máxima de la bola
                if (ballSpeed.x >= 15) {
                    ballSpeed.x = 15;
                }
                if (ballSpeed.y >= 15) {
                    ballSpeed.y = 15;
                }
                
                // TODO: Player movement logic.......................(0.2p)
                if (!pause) {
                    
                    //Player 1
                   if (IsKeyDown(KEY_W)) {
                        if (player.y >= 0) {
                            player.y -= playerSpeedY;
                        }   
                    }
                    
                    if (IsKeyDown(KEY_S)) {
                        if (player.y <= screenHeight-player.height) { 
                            player.y += playerSpeedY;
                        }       
                    } 

                    //Player 2
                    if (IsKeyDown(KEY_UP)) {
                        if (enemy.y >= 0) {
                            enemy.y -= enemySpeedY;
                        }   
                    }
                    
                    if (IsKeyDown(KEY_DOWN)) {
                        if (enemy.y <= screenHeight-enemy.height) { 
                            enemy.y += enemySpeedY;
                        }       
                    }
                                
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                    if (CheckCollisionCircleRec(ballPosition, ballRadius, player)) {
                       // PlaySound(ColSound);
                        if (abs(ballSpeed.x) < maxVelocity){
                            ballSpeed.x *=-1.2;
                            ballSpeed.y *=1.2;
                        } else {            
                            ballSpeed.x *=-1;
                            ballSpeed.y *=1;
                        }
                    }
                
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                    if (CheckCollisionCircleRec(ballPosition, ballRadius, enemy)) {
                        //PlaySound(ColSound);
                        if (abs(ballSpeed.x) < maxVelocity){
                            ballSpeed.x *=-1.1;
                            ballSpeed.y *=1.1;
                        } else {            
                            ballSpeed.x *=-1;
                            ballSpeed.y *=1;
                        }
                        
                    }
                
                // TODO: Collision detection (ball-limits) logic.....(1p)
                    if (ballPosition.x - ballRadius < 0) { 
                        //PlaySound(GolSound);
                        ballPosition.x = screenWidth/2;
                        ballPosition.y = screenHeight/2;
                        ballSpeed.x = InitialVelocity;
                        ballSpeed.y = InitialVelocity;
                        lifeRect1.width -= gol;
                        
                    }
                    if (ballPosition.x + ballRadius > screenWidth) {
                        //PlaySound(GolSound);
                        ballPosition.x = screenWidth/2;
                        ballPosition.y = screenHeight/2;
                        ballSpeed.x = -InitialVelocity;
                        ballSpeed.y = InitialVelocity; 
                        lifeRect2.width -= gol;
                    }
                    
                    if (ballPosition.y - ballRadius < 0) {                   
                        ballSpeed.y *=-1;   
                    }
                    if (ballPosition.y + ballRadius > screenHeight) {
                        ballSpeed.y *=-1;   
                    }
                
                // TODO: Life bars decrease logic....................(1p)
                
                //Está puesto en el collision detection

                // TODO: Time counter logic..........................(0.2p)             
                    framesCounter++;
                    if (framesCounter >= 60)
                    {
                        secondsCounter--;
                        framesCounter = 0;
                    }
                

                // TODO: Game ending logic...........................(0.2p)
                    if (secondsCounter <= 0) screen = ENDINGTWO;
                    if (lifeRect1.width <= 0) screen = ENDINGTWO;
                    if (lifeRect2.width <= 0) screen = ENDINGTWO;
                    
                }
                
                // TODO: Pause button logic..........................(0.2p)
                if (IsKeyPressed(KEY_P)){
                    //PlaySound(PauseSound);
                    pause = !pause;
                }
                
            } break;
            case FOUR:
            { 
                // Update GAMEPLAY screen data here!
                    framesCounter++;
                    if (framesCounter % 30 == 0)
                    {
                        framesCounter = 0;
                        blink = !blink;
                    }

                // TODO: Ball movement logic.........................(0.2p)
                if (!pause){
                    ballPosition.x += ballSpeed.x;
                    ballPosition.y += ballSpeed.y;
                }
                
                //Velocidad máxima de la bola
                if (ballSpeed.x >= 15) {
                    ballSpeed.x = 15;
                }
                if (ballSpeed.y >= 15) {
                    ballSpeed.y = 15;
                }
                
                // TODO: Player movement logic.......................(0.2p)
                if (!pause) {
                    
                    //Player 1
                   if (IsKeyDown(KEY_W)) {
                        if (player1.y >= 0) {
                            player1.y -= player1SpeedY;
                        }   
                    }
                    
                    if (IsKeyDown(KEY_S)) {
                        if (player1.y <= screenHeight/2-player1.height) { 
                            player1.y += player1SpeedY;
                        }       
                    } 

                    //Player 3
                    if (IsKeyDown(KEY_UP)) {
                        if (player3.y >= 0) {
                            player3.y -= player3SpeedY;
                        }   
                    }
                    
                    if (IsKeyDown(KEY_DOWN)) {
                        if (player3.y <= screenHeight/2-player3.height) { 
                            player3.y += player3SpeedY;
                        }       
                    }
                    
                    //Player 2
                    if (IsKeyDown(KEY_R)) {
                        if (player2.y >= screenHeight/2) {
                            player2.y -= player2SpeedY;
                        }   
                    }
                    
                    if (IsKeyDown(KEY_F)) {
                        if (player2.y <= screenHeight-player2.height) { 
                            player2.y += player2SpeedY;
                        }       
                    }
                    
                    //Player 4
                    if (IsKeyDown(KEY_O)) {
                        if (player4.y >= screenHeight/2) {
                            player4.y -= player4SpeedY;
                        }   
                    }
                    
                    if (IsKeyDown(KEY_L)) {
                        if (player4.y <= screenHeight-player4.height) { 
                            player4.y += player4SpeedY;
                        }       
                    }                               
                
                    //P1 Collision
                    if (CheckCollisionCircleRec(ballPosition, ballRadius, player1)) {
                       // PlaySound(ColSound);
                        if (abs(ballSpeed.x) < maxVelocity){
                            ballSpeed.x *=-1.2;
                            ballSpeed.y *=1.2;
                        } else {            
                            ballSpeed.x *=-1;
                            ballSpeed.y *=1;
                        }
                    }
                    
                    //P2 Collision
                    if (CheckCollisionCircleRec(ballPosition, ballRadius, player2)) {
                           // PlaySound(ColSound);
                            if (abs(ballSpeed.x) < maxVelocity){
                                ballSpeed.x *=-1.2;
                                ballSpeed.y *=1.2;
                            } else {            
                                ballSpeed.x *=-1;
                                ballSpeed.y *=1;
                            }
                        }
                        
                        //P3 Collision
                        if (CheckCollisionCircleRec(ballPosition, ballRadius, player3)) {
                           // PlaySound(ColSound);
                            if (abs(ballSpeed.x) < maxVelocity){
                                ballSpeed.x *=-1.2;
                                ballSpeed.y *=1.2;
                            } else {            
                                ballSpeed.x *=-1;
                                ballSpeed.y *=1;
                            }
                        }
                        
                        //P4 Collision
                        if (CheckCollisionCircleRec(ballPosition, ballRadius, player4)) {
                           // PlaySound(ColSound);
                            if (abs(ballSpeed.x) < maxVelocity){
                                ballSpeed.x *=-1.2;
                                ballSpeed.y *=1.2;
                            } else {            
                                ballSpeed.x *=-1;
                                ballSpeed.y *=1;
                            }
                        }
                
                // TODO: Collision detection (ball-limits) logic.....(1p)
                    if (ballPosition.x - ballRadius < 0 && ballPosition.y - ballRadius < screenHeight/2) { 
                       // PlaySound(GolSound);
                        ballPosition.x = screenWidth/2;
                        ballPosition.y = screenHeight/2;
                        ballSpeed.x = InitialVelocity;
                        ballSpeed.y = InitialVelocity;
                        lifeRect1.width -= gol;                        
                    }
                    
                    if (ballPosition.x - ballRadius < 0 && ballPosition.y - ballRadius > screenHeight/2) { 
                        //PlaySound(GolSound);
                        ballPosition.x = screenWidth/2;
                        ballPosition.y = screenHeight/2;
                        ballSpeed.x = InitialVelocity;
                        ballSpeed.y = InitialVelocity;
                        lifeRect3.width -= gol;                        
                    }
                    
                    if (ballPosition.x + ballRadius > screenWidth && ballPosition.y + ballRadius < screenHeight/2) {
                       // PlaySound(GolSound);
                        ballPosition.x = screenWidth/2;
                        ballPosition.y = screenHeight/2;
                        ballSpeed.x = -InitialVelocity;
                        ballSpeed.y = InitialVelocity; 
                        lifeRect2.width -= gol;
                    }
                    
                    if (ballPosition.x + ballRadius > screenWidth && ballPosition.y + ballRadius > screenHeight/2) {
                       // PlaySound(GolSound);
                        ballPosition.x = screenWidth/2;
                        ballPosition.y = screenHeight/2;
                        ballSpeed.x = -InitialVelocity;
                        ballSpeed.y = InitialVelocity; 
                        lifeRect4.width -= gol;
                    }
                    
                    if (ballPosition.y - ballRadius < 0) {                   
                        ballSpeed.y *=-1;   
                    }
                    if (ballPosition.y + ballRadius > screenHeight) {
                        ballSpeed.y *=-1;   
                    }
                
                // TODO: Life bars decrease logic....................(1p)
                
                //Está puesto en el collision detection

                // TODO: Time counter logic..........................(0.2p)             
                    framesCounter++;
                    if (framesCounter >= 60)
                    {
                        secondsCounter--;
                        framesCounter = 0;
                    }
                

                // TODO: Game ending logic...........................(0.2p)
                    if (secondsCounter <= 0) screen = ENDINGFOUR;
                    if (lifeRect1.width <= 0) screen = ENDINGFOUR;
                    if (lifeRect2.width <= 0) screen = ENDINGFOUR;
                    if (lifeRect3.width <= 0) screen = ENDINGFOUR;
                    if (lifeRect4.width <= 0) screen = ENDINGFOUR;
                }
                
                // TODO: Pause button logic..........................(0.2p)
                if (IsKeyPressed(KEY_P)){
                   // PlaySound(PauseSound);
                    pause = !pause;
                }
                
            } break;
            
            case ENDINGONE: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                if (IsKeyPressed(KEY_R)){
                    secondsCounter = 99;
                    lifeRect1 = fillRect1;
                    lifeRect2 = fillRect2;
                    lifeRect3 = fillRect3;
                    lifeRect4 = fillRect4;
                    screen = PLAYERS;
                }
                
            } break;
            
            case ENDINGTWO: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                if (IsKeyPressed(KEY_R)){
                    secondsCounter = 99;
                    lifeRect1 = fillRect1;
                    lifeRect2 = fillRect2;
                    lifeRect3 = fillRect3;
                    lifeRect4 = fillRect4;
                    screen = PLAYERS;
                }
                
            } break;
            
            case ENDINGFOUR: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                if (IsKeyPressed(KEY_R)){
                    secondsCounter = 99;
                    lifeRect1 = fillRect1;
                    lifeRect2 = fillRect2;
                    lifeRect3 = fillRect3;
                    lifeRect4 = fillRect4;
                    screen = PLAYERS;
                }
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    Txt = "Made by: Eva Agramunt";
                    TamanoTxt = MeasureTextEx(fontTTF, Txt, 70, 0);               
                    DrawTextEx(fontTTF, Txt, (Vector2){screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y}, 70, 0, logoColor);
                    
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    ClearBackground(BLACK);
                    // TODO: Draw Title..............................(0.2p)
                    Txt = "THE PONG";
                    TamanoTxt = MeasureTextEx(fontTTF, Txt, 180, 0);               
                    DrawTextEx(fontTTF, Txt, (Vector2){screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y}, 180, 0, RAYWHITE);
                    
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    Txt = "Press ENTER to Start";
                    TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                    DrawTextEx(fontTTF, Txt, (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2}, 50, 0, RAYWHITE);     
                    
                    
                    
                } break;              
                case PLAYERS: 
                {
                    secondsCounter = 99;
                    ClearBackground(BLACK);
                    
                    Txt = "How many players are playing today?";
                    TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);               
                    DrawTextEx(fontTTF, Txt, (Vector2){screenWidth/2 - TamanoTxt.x/2, 10 + TamanoTxt.y}, 50, 0, RAYWHITE);

                    Txt = "[O] One Player  ";
                    TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                    DrawTextEx(fontTTF, Txt, (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y}, 40, 0, RAYWHITE);     
                    
                    
                    Txt = "[T] Two Players";
                    TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                    DrawTextEx(fontTTF, Txt, (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y/2}, 40, 0, RAYWHITE);     
                    
                    
                    Txt = "[F] Four Players";
                    TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                    DrawTextEx(fontTTF, Txt, (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2}, 40, 0, RAYWHITE);     
                    
                } break;
                
                case LEVEL: 
                {
                    
                    ClearBackground(BLACK);
                    
                    Txt = "Select the difficulty";
                    TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);               
                    DrawTextEx(fontTTF, Txt, (Vector2){screenWidth/2 - TamanoTxt.x/2, 10 + TamanoTxt.y}, 50, 0, RAYWHITE);

                    Txt = "[E] Easy";
                    TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                    DrawTextEx(fontTTF, Txt, (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y}, 40, 0, RAYWHITE);     
                    
                    
                    Txt = "[N] Normal";
                    TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                    DrawTextEx(fontTTF, Txt, (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y/2}, 40, 0, RAYWHITE);     
                    
                    
                    Txt = "[H] Hard";
                    TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                    DrawTextEx(fontTTF, Txt, (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2}, 40, 0, RAYWHITE);     
                    
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    DrawCircleV (ballPosition, ballRadius, BLACK);
                    DrawLine(iaLinex,0,iaLinex,screenHeight, RAYWHITE);
                    
                    DrawLine (screenWidth/2, 0, screenWidth/2, screenHeight, BLACK);
                    
                    // TODO: Draw player and enemy...................(0.2p)
                    DrawRectangleRec(player, BLACK);
                    DrawRectangleRec(enemy, BLACK);
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    DrawRectangleRec (backRect1, BLACK);
                    DrawRectangleRec (fillRect1, RED);
                    DrawRectangleRec (lifeRect1, lifeColor1);
                    
                    DrawRectangleRec (backRect2, BLACK);
                    DrawRectangleRec (fillRect2, RED);
                    DrawRectangleRec (lifeRect2, lifeColor2);
                    // TODO: Draw time counter.......................(0.5p)
                    
                    DrawText(FormatText("%i", secondsCounter), screenWidth/2 - 25, 10, 50, BLACK);
                    
                    // TODO: Draw pause message when required........(0.5p)
                    if (pause){
                       DrawRectangleV (RecPausePos, RecPause, BLACK);
                      
                      //Mide el tamaño del texto
                       Txt = "PAUSE";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 200, 0);
                       //Dibuja el texto                
                       DrawTextEx(fontTTF, Txt, (Vector2){screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y}, 200, 0, BLUE);
                       
                       //Mide el tamaño del texto
                       Txt = "Press P to continue";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                       //Dibuja el texto
                       if (blink) { 
                        DrawTextEx(fontTTF, "Press P to continue", (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2}, 50, 0, BLUE);
                        } 
                    }
                    
                } break;
                case TWO:
                { 
                    // Draw GAMEPLAY screen here!
                    DrawCircleV (ballPosition, ballRadius, BLACK);
                    DrawLine (screenWidth/2, 0, screenWidth/2, screenHeight, BLACK);
                    
                    // TODO: Draw player and enemy...................(0.2p)
                    DrawRectangleRec(player, BLACK);
                    DrawRectangleRec(enemy, BLACK);
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    DrawRectangleRec (backRect1, BLACK);
                    DrawRectangleRec (fillRect1, RED);
                    DrawRectangleRec (lifeRect1, lifeColor1);
                    
                    DrawRectangleRec (backRect2, BLACK);
                    DrawRectangleRec (fillRect2, RED);
                    DrawRectangleRec (lifeRect2, lifeColor2);
                    // TODO: Draw time counter.......................(0.5p)
                    
                    DrawText(FormatText("%i", secondsCounter), screenWidth/2 - 25, 10, 50, BLACK);
                    
                    // TODO: Draw pause message when required........(0.5p)
                    if (pause){
                       DrawRectangleV (RecPausePos, RecPause, BLACK);
                      
                      //Mide el tamaño del texto
                       Txt = "PAUSE";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 200, 0);
                       //Dibuja el texto                
                       DrawTextEx(fontTTF, Txt, (Vector2){screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y}, 200, 0, BLUE);
                       
                       //Mide el tamaño del texto
                       Txt = "Press P to continue";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                       //Dibuja el texto
                       if (blink) { 
                        DrawTextEx(fontTTF, "Press P to continue", (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2}, 50, 0, BLUE);
                        } 
                    }               
                } break;
                
                case FOUR:
                { 
                    DrawCircleV (ballPosition, ballRadius, BLACK);
                    DrawLine (screenWidth/2, 0, screenWidth/2, screenHeight, BLACK);
                    DrawLine (0, screenHeight/2, screenWidth, screenHeight/2, BLACK);
                    
                    // TODO: Draw player and enemy...................(0.2p)
                    DrawRectangleRec(player1, BLACK);
                    DrawRectangleRec(player2, BLACK);
                    DrawRectangleRec(player3, BLACK);
                    DrawRectangleRec(player4, BLACK);
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    DrawRectangleRec (backRect1, BLACK);
                    DrawRectangleRec (fillRect1, RED);
                    DrawRectangleRec (lifeRect1, lifeColor1);
                    
                    DrawRectangleRec (backRect2, BLACK);
                    DrawRectangleRec (fillRect2, RED);
                    DrawRectangleRec (lifeRect2, lifeColor2);
                    
                    DrawRectangleRec (backRect3, BLACK);
                    DrawRectangleRec (fillRect3, RED);
                    DrawRectangleRec (lifeRect3, lifeColor2);
                    
                    DrawRectangleRec (backRect4, BLACK);
                    DrawRectangleRec (fillRect4, RED);
                    DrawRectangleRec (lifeRect4, lifeColor2);
                    // TODO: Draw time counter.......................(0.5p)
                    
                    DrawText(FormatText("%i", secondsCounter), screenWidth/2 - 25, 10, 50, BLACK);
                    
                    // TODO: Draw pause message when required........(0.5p)
                    if (pause){
                       DrawRectangleV (RecPausePos, RecPause, BLACK);
                      
                      //Mide el tamaño del texto
                       Txt = "PAUSE";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 200, 0);
                       //Dibuja el texto                
                       DrawTextEx(fontTTF, Txt, (Vector2){screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y}, 200, 0, BLUE);
                       
                       //Mide el tamaño del texto
                       Txt = "Press P to continue";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                       //Dibuja el texto
                       if (blink) { 
                        DrawTextEx(fontTTF, "Press P to continue", (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2}, 50, 0, BLUE);
                        } 
                    }
   
                } break;
                
                case ENDINGONE: 
                {
                    // Draw END screen here!
                    ClearBackground(BLACK);
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    if (lifeRect1.width > lifeRect2.width) {      
                       Txt = "YOU WIN";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 200, 0);               
                       DrawTextEx(fontTTF, Txt, (Vector2){screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y}, 200, 0, GREEN);
                       
                       Txt = "Press R to reestart or ESC to exit";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                       DrawTextEx(fontTTF, Txt, (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2}, 50, 0, RAYWHITE);     
                       
                    } else if (lifeRect1.width < lifeRect2.width) {
                       Txt = "YOU LOOSE";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 200, 0);            
                       DrawTextEx(fontTTF, Txt, (Vector2){screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y}, 200, 0, RED);
                       
                       Txt = "Press R to reestart or ESC to exit";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                       DrawTextEx(fontTTF, Txt, (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2}, 50, 0, RAYWHITE);     
                        
                    } else {
                       Txt = "DRAW";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 200, 0);             
                       DrawTextEx(fontTTF, Txt, (Vector2){screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y}, 200, 0, BLUE);
                       
                       Txt = "Press R to reestart or ESC to exit";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                       DrawTextEx(fontTTF, Txt, (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2}, 50, 0, RAYWHITE);     
                    }
                    
                } break;
                
                 case ENDINGTWO: 
                {
                    // Draw END screen here!
                    ClearBackground(BLACK);
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    if (lifeRect1.width > lifeRect2.width) {      
                       Txt = "PLAYER 1 WINS";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 100, 0);               
                       DrawTextEx(fontTTF, Txt, (Vector2){screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y}, 100, 0, GREEN);
                       
                       Txt = "Press R to reestart or ESC to exit";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                       DrawTextEx(fontTTF, Txt, (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2}, 50, 0, RAYWHITE);     
                       
                    } else if (lifeRect1.width < lifeRect2.width) {
                       Txt = "PLAYER 2 WINS";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 100, 0);            
                       DrawTextEx(fontTTF, Txt, (Vector2){screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y}, 100, 0, RED);
                       
                       Txt = "Press R to reestart or ESC to exit";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                       DrawTextEx(fontTTF, Txt, (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2}, 50, 0, RAYWHITE);     
                        
                    } else {
                       Txt = "DRAW";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 200, 0);             
                       DrawTextEx(fontTTF, Txt, (Vector2){screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y}, 200, 0, BLUE);
                       
                       Txt = "Press R to reestart or ESC to exit";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                       DrawTextEx(fontTTF, Txt, (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2}, 50, 0, RAYWHITE);     
                    }
                    
                } break;
                
                 case ENDINGFOUR: 
                {
                    // Draw END screen here!
                    ClearBackground(BLACK);
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    if (lifeRect1.width > lifeRect2.width > lifeRect3.width > lifeRect4.width) {      
                       Txt = "PLAYER 1 WINS";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 100, 0);               
                       DrawTextEx(fontTTF, Txt, (Vector2){screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y}, 100, 0, GREEN);
                       
                       Txt = "Press R to reestart \n Press ESC to exit";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                       DrawTextEx(fontTTF, Txt, (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2}, 50, 0, RAYWHITE);     
                       
                    } else if (lifeRect1.width < lifeRect2.width > lifeRect3.width > lifeRect4.width) {
                       Txt = "PLAYER 2 WINS";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 100, 0);            
                       DrawTextEx(fontTTF, Txt, (Vector2){screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y}, 100, 0, RED);
                       
                       Txt = "Press R to reestart \n Press ESC to exit";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                       DrawTextEx(fontTTF, Txt, (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2}, 50, 0, RAYWHITE);     
                        
                    } else if (lifeRect3.width > lifeRect2.width > lifeRect1.width > lifeRect4.width) {
                       Txt = "PLAYER 3 WINS";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 100, 0);            
                       DrawTextEx(fontTTF, Txt, (Vector2){screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y}, 100, 0, RED);
                       
                       Txt = "Press R to reestart \n Press ESC to exit";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                       DrawTextEx(fontTTF, Txt, (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2}, 50, 0, RAYWHITE);     
                        
                    } else if (lifeRect4.width > lifeRect2.width > lifeRect1.width > lifeRect3.width) {
                       Txt = "PLAYER 4 WINS";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 100, 0);            
                       DrawTextEx(fontTTF, Txt, (Vector2){screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y}, 100, 0, RED);
                       
                       Txt = "Press R to reestart \n Press ESC to exit";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                       DrawTextEx(fontTTF, Txt, (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2}, 50, 0, RAYWHITE);     
                        
                    } else {
                       Txt = "DRAW";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 150, 0);             
                       DrawTextEx(fontTTF, Txt, (Vector2){screenWidth/2 - TamanoTxt.x/2, screenHeight/2 - TamanoTxt.y}, 150, 0, BLUE);
                       
                       Txt = "Press R to reestart \n Press ESC to exit";
                       TamanoTxt = MeasureTextEx(fontTTF, Txt, 50, 0);
                       DrawTextEx(fontTTF, Txt, (Vector2) {screenWidth/2 - TamanoTxt.x/2, screenHeight/2}, 50, 0, RAYWHITE);     
                    }
                    
                } break;
                
                
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
   /* UnloadFont(fontTTF);
    UnloadSound (ColSound);
    UnloadSound (GolSound);
    UnloadSound (PauseSound);*/
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}